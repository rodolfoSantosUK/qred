package com.smooothies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmooothiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmooothiesApplication.class, args);
    }

}
