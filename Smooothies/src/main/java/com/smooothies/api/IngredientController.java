package com.smooothies.api;

import com.smooothies.service.SmoothieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    @Autowired
    private SmoothieService smoothieService;


    @PostMapping
    public List<String> smoothie (@RequestParam(value = "ingredient" ) String ingredient ) throws Exception {
    return  smoothieService.getIngredientsFinal(ingredient);
    }


}

