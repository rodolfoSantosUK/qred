package com.smooothies.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UnsuportedMethodException extends RuntimeException {

        public UnsuportedMethodException(String exception ) {
            super(exception);
        }

}
