package com.smooothies.service;

import com.smooothies.util.SmoothieUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service

public class SmoothieService {

    private SmoothieUtils smoothieUtils = new SmoothieUtils();

    private List<String> chosenIngredients = new ArrayList<>();

    public List<String> getIngredientsFinal(String requests) {

        Map<String, Set<String>> ingredientsMap = SmoothieUtils.getIngredientsMap(requests);

        if (ingredientsMap.size() > 0) {

            if (smoothieUtils.getMapaMenu().get(Arrays.asList(requests.split(",")).get(0).trim()) == null) {
                throw new IllegalArgumentException("Not allowed to inform non-existent ingredients");

            } else if (!isValidaToRemove(SmoothieUtils.TO_REMOVE, ingredientsMap)) {

                throw new IllegalArgumentException("Not allowed to inform non-existent ingredients");

            }
        } else {

                throw new IllegalArgumentException("Not allowed to inform non-existent ingredients");

        }

        buildChosenIngredients(ingredientsMap);

        List<String> response = buildResponseItemsMenu(ingredientsMap, Arrays.asList(requests.split(",")).get(0).trim());

        return response;

    }


    private boolean isValidaToRemove(String option, Map<String, Set<String>> ingredientsMap) {

        return (ingredientsMap.get(option) != null &&
                SmoothieUtils.getAllIngredients().containsAll(ingredientsMap.get(option)));

    }


    private boolean isValidaToUse(String option, Map<String, Set<String>> ingredientsMap) {

        return (ingredientsMap.get(option) != null &&
                SmoothieUtils.getAllIngredients().containsAll(ingredientsMap.get(option)));

    }


    private void buildChosenIngredients(Map<String, Set<String>> ingredientsMap) {
        ingredientsMap.forEach((k, v) -> {
            v.forEach(x -> chosenIngredients.add(x));

        });
    }

    private List<String> buildResponseItemsMenu(Map<String, Set<String>> ingredientsMap, String keyMenu) {


        Set<String> itemsMenu = new HashSet<>(smoothieUtils.getMapaMenu().get(keyMenu));

        if (ingredientsMap.get(SmoothieUtils.TO_REMOVE) != null)
            itemsMenu.removeAll(ingredientsMap.get(SmoothieUtils.TO_REMOVE));

        if (ingredientsMap.get(SmoothieUtils.TO_USE) != null)

            itemsMenu.addAll(ingredientsMap.get(SmoothieUtils.TO_USE));
        return itemsMenu.stream().sorted().collect(Collectors.toList());

    }


}
