package com.smooothies.util;

import java.util.*;
import java.util.stream.Collectors;


public class SmoothieUtils {

    private Map <String, List<String>> mapaMenu = new HashMap<String, List<String>>();
    public  static final String TO_REMOVE = "ToRemove";
    public  static final String TO_USE = "ToUse";
    private static final List<String> allIngredients = new ArrayList<>();

    public static Map<String, Set<String>> getIngredientsMap(String requests) {

        List<String> lista =  new LinkedList<String> (Arrays.asList(requests.split(",")));
        lista.remove(0);

        return lista.stream().collect(
                Collectors.groupingBy(l -> {
                            if(l.trim().startsWith("-")) {
                                return SmoothieUtils.TO_REMOVE;
                            } else {
                                return SmoothieUtils.TO_USE;
                            }
                        }  ,
                        Collectors.mapping( z -> z.trim().startsWith("-") ?  z.trim().replace("-","")
                                : new StringBuilder(z.trim()).toString()
                                , Collectors.toSet())
                )
        );

    }

    public  Map<String, List<String>> getMapaMenu() {

        mapaMenu.put("Classic", Arrays.asList("strawberry", "banana", "pineapple", "mango", "peach", "honey"));
        mapaMenu.put("Freezie", Arrays.asList("blackberry", "blueberry", "black currant", "grape juice", "frozen yogurt"));
        mapaMenu.put("Greenie", Arrays.asList("green apple", "lime", "avocado", "spinach", "ice", "apple juice"));
        mapaMenu.put("Just Desserts", Arrays.asList("banana", "ice cream", "chocolate", "peanut", "cherry"));


        mapaMenu.forEach((k, v) -> {
            v.forEach( x -> allIngredients.add(x));

        });

       return mapaMenu;


   }

    public static List<String> getAllIngredients() {
       return  allIngredients ;
    }


}
