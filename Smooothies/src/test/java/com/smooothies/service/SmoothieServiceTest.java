package com.smooothies.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class SmoothieServiceTest {

    @InjectMocks
    private SmoothieService smoothieService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void might_Create_Classic_Ingredient_List() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Classic ,-banana,-mango,honey ");

      assertThat(ingredientsFinal.containsAll(Arrays.asList( "strawberry", "pineapple",  "peach", "honey")));

    }


    @Test
    public void might_Create_Classic_Without_Key () throws IllegalArgumentException {

        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Not allowed to inform non-existent ingredients");

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("rodolfo");


    }

    @Test
    public void might_Create_Classic_Ingredient_With_Space_Over_Main_Menu() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("   Classic   ,-banana,-mango,honey ");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "strawberry", "pineapple",  "peach", "honey")));

    }

    @Test
    public void might_Create_Classic_Ingredient_With_Space_Over_Ingredients() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("   Classic   ,  -banana , -mango ,  honey ");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "strawberry", "pineapple",  "peach", "honey")));

    }


    @Test
    public void might_Create_Freezie_Ingredient_List() throws IllegalArgumentException {


        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Freezie,-black currant,-blueberry");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "blackberry", "grape juice", "frozen yogurt" )));
        assertThat(ingredientsFinal.size()).isEqualTo(3);
    }


    @Test
    public void might_Create_Greenie_Ingredient_List() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Greenie,-avocado,spinach");
         assertThat(ingredientsFinal.containsAll(Arrays.asList( "green apple", "lime",  "ice", "apple juice", "spinach")));
        assertThat(ingredientsFinal.size()).isEqualTo(5);
    }

    @Test
    public void might_Create_JustDesserts_Ingredient_List() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Just Desserts,-ice cream");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "banana", "chocolate",  "peanut", "cherry")));
        assertThat(ingredientsFinal.size()).isEqualTo(4);
    }

    @Test
    public void might_Create_Classic_With_Ingredient_From_JustDesserts() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Classic,-banana,-mango,honey,chocolate,cherry");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "strawberry", "pineapple",  "peach", "honey", "chocolate", "cherry")));
        assertThat(ingredientsFinal.size()).isEqualTo(6);
    }


    @Test
    public void might_Create_Greenie_Ingredient_With_Ingredient_From_Freezie () throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Greenie,-avocado,spinach,blackberry");
        assertThat(ingredientsFinal.containsAll(Arrays.asList( "green apple", "lime",  "ice", "apple juice", "spinach", "blackberry")));
        assertThat(ingredientsFinal.size()).isEqualTo(6);
    }

    @Test
    public void might_Create_JustDesserts_Ingredient_With_Ingredient_From_Greenie() throws IllegalArgumentException {

        List<String> ingredientsFinal = smoothieService.getIngredientsFinal("Just Desserts,-ice cream,avocado");

        assertThat(ingredientsFinal.containsAll(Arrays.asList( "banana", "chocolate",  "peanut", "cherry", "avocado")));
        assertThat(ingredientsFinal.size()).isEqualTo(5);
    }




}